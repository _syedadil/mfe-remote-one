# Stage 1: Build

FROM node:16-alpine AS builder

RUN mkdir -p /app
WORKDIR /app

COPY package.json package-lock.json /app/
RUN npm ci
COPY . /app/

RUN npm run build


# Stage 2

FROM nginx:1.23.3-alpine

COPY --from=builder /app/dist/ /usr/share/nginx/html

COPY --from=builder /app/nginx/default.conf /etc/nginx/conf.d/
